# Configure the Azure Provider
provider "opc" {

  user              = "${var.oracle_user}"
  password          = "${var.oracle_password}"
  identity_domain   = "${var.oracle_identity_domain}"
  endpoint          = "${var.oracle_endpoint}"
}

