## STEP 1 - Create your default AWS credentials under your computer profile
set ~/.aws/credentials  
export AWS_PROFILE=terraform  
  
## STEP 2 - Create your security credentials on AWS under your username  
  
## STEP 3 - Create your secrets file. Try to keep the same name to avoid conflicts when commiting to GIT
```echo << EOF > 002-secrets.tf  
variable "aws_access_key" {  
    default = "<KEY>"  
}  
variable "aws_secret_key" {  
    default = "<KEY>"  
}  
EOF ``` 

## STEP 4 - Test your backend infrastructure by running
terraform init

#STEP 5 - If that works, proceed with the plan and apply commands whenever ready with new code. If not, make sure your ~/aws/credentials file is created as mentioned in STEP 1
terraform plan  
terraform apply  

#Here are some quick commands for GIT.
Git Quick Commands:  
git push origin master  
git pull  
git commit  
git fetch  
git diff  
git add .  

#### You can also use Sourcetree for git purposes available at https://www.sourcetreeapp.com

Documentation for AWS:
https://www.terraform.io/docs/providers/aws/index.html

Documentation for Azure:
https://www.terraform.io/docs/providers/azurerm/index.html

Documentation for Google Cloud Platform:
https://www.terraform.io/docs/providers/google/index.html

Documentation for VMWare vSphere:
https://www.terraform.io/docs/providers/vsphere/index.html

#### Authentication on Azure:

For azure, you will need to download the azure CLI to be able to get a client ID and a client Secret

1. az cloud set --name AzureCloud
2. az login
3. az account list

The output will be similar to below
```
[
  {
    "cloudName": "AzureCloud",
    "id": "47bad636-0982-4da7-b245-aba5478532cf",
    "isDefault": true,
    "name": "Microsoft Azure Enterprise",
    "state": "Enabled",
    "tenantId": "238fa307-adf1-42ca-acd8-26dc598490f7",
    "user": {
      "name": "41dc2eba-fc5f-4cdb-9fa7-3dfee916cbb2",
      "type": "servicePrincipal"
    }
  }
]```

* az account set --subscription="SUBSCRIPTION_ID" where SUBSCRIPTION_ID = id from the above
* az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/SUBSCRIPTION_ID" where SUBSCRIPTION_ID = id from the above
 
The output will be similar to below
```{
  "appId": "00000000-0000-0000-0000-000000000000",
  "displayName": "azure-cli-2017-06-05-10-41-15",
  "name": "http://azure-cli-2017-06-05-10-41-15",
  "password": "0000-0000-0000-0000-000000000000",
  "tenant": "00000000-0000-0000-0000-000000000000"
}```

Insert the variables in your secret file:
```variable ARM_CLIENT_ID { 
    default = "<appId>"
} ```
```variable ARM_CLIENT_SECRET { 
    default = "<password>" 
}```
```variable ARM_SUBSCRIPTION_ID { 
    default = "<same ID as earlier>" 
}```
```variable ARM_TENANT_ID { 
    default = "<tenant>" 
}```