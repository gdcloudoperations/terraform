resource "aws_vpc" "vpc-4f395737" {
    cidr_block           = "172.16.0.0/16"
    enable_dns_hostnames = false
    enable_dns_support   = true
    instance_tenancy     = "default"

    tags {
        "AWSServiceAccount" = "697148468905"
    }
}

resource "aws_vpc" "cold-1-dr-vpc" {
    cidr_block           = "172.30.0.0/16"
    enable_dns_hostnames = true
    enable_dns_support   = true
    instance_tenancy     = "default"

    tags {
        "Name" = "cold-1-dr-vpc"
        "Mode" = "Cold"
        "Terraform" = "true"
        "Environment" = "DR"
    }
}

resource "aws_vpc" "prod-active-vpc" {
    cidr_block           = "172.28.0.0/16"
    enable_dns_hostnames = true
    enable_dns_support   = true
    instance_tenancy     = "default"

    tags {
        "Terraform" = "true"
        "Mode" = "Active"
        "Environment" = "PRODUCTION"
        "Name" = "prod-active-vpc"
    }
}

resource "aws_vpc" "cold-2-dr-vpc" {
    cidr_block           = "172.31.0.0/16"
    enable_dns_hostnames = true
    enable_dns_support   = true
    instance_tenancy     = "default"

    tags {
        "Mode" = "Cold"
        "Name" = "cold-2-dr-vpc"
        "Environment" = "DR"
        "Terraform" = "true"
    }
}

resource "aws_vpc" "default" {
    cidr_block           = "172.30.0.0/16"
    enable_dns_hostnames = true
    enable_dns_support   = true
    instance_tenancy     = "default"

    tags {
        "Name" = "default"
    }
}

