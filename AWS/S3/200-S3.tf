resource "aws_s3_bucket" "Mtl" {
    bucket = "Mtl"
    acl    = "private"
}

resource "aws_s3_bucket" "creativeupload" {
    bucket = "creativeupload"
    acl    = "private"
}

resource "aws_s3_bucket" "data-lake-us-east-1-195767839826" {
    bucket = "data-lake-us-east-1-195767839826"
    acl    = "private"
    cors_rule {
        allowed_headers = ["*"]
        allowed_methods = ["HEAD", "GET", "PUT", "POST"]
        allowed_origins = ["*"]
  }
}

resource "aws_s3_bucket" "dms-195767839826-c5cbgs7gao0t" {
    bucket = "dms-195767839826-c5cbgs7gao0t"
    acl    = "private"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "BucketPolicy",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::195767839826:role/dms-access-for-tasks"
      },
      "Action": [
        "s3:GetObject",
        "s3:PutObject",
        "s3:DeleteObject",
        "s3:GetObjectVersion",
        "s3:GetBucketPolicy",
        "s3:PutBucketPolicy",
        "s3:DeleteBucketPolicy",
        "s3:ListBucket",
        "s3:GetBucketLocation",
        "s3:DeleteBucket"
      ],
      "Resource": [
        "arn:aws:s3:::dms-195767839826-c5cbgs7gao0t/*",
        "arn:aws:s3:::dms-195767839826-c5cbgs7gao0t"
      ]
    }
  ]
}
POLICY
}

resource "aws_s3_bucket" "dyn-gigya" {
    bucket = "dyn-gigya"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-amis" {
    bucket = "dynamite-amis"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-bastion-keys" {
    bucket = "dynamite-bastion-keys"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-commvault-tape01" {
    bucket = "dynamite-commvault-tape01"
    acl    = "private"
    tags = {
        Prod = "backup"
    }
}

resource "aws_s3_bucket" "dynamite-commvault-tape02" {
    bucket = "dynamite-commvault-tape02"
    acl    = "private"
    tags = {
        Prod = "backup"
    }
}

resource "aws_s3_bucket" "dynamite-configs" {
    bucket = "dynamite-configs"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-db-backups" {
    bucket = "dynamite-db-backups"
    acl    = "private"
    tags = {
        PROD = "backup bucket for WMS"
    }
}

resource "aws_s3_bucket" "dynamite-domains-backup" {
    bucket = "dynamite-domains-backup"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-dr-restores" {
    bucket = "dynamite-dr-restores"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-dr-services-dev-serverlessdeploymentbuck-1o8eg8ogf96b2" {
    bucket = "dynamite-dr-services-dev-serverlessdeploymentbuck-1o8eg8ogf96b2"
    acl    = "private"
    tags = {
        STAGE = "dev"
    }
}

resource "aws_s3_bucket" "dynamite-gigya-export" {
    bucket = "dynamite-gigya-export"
    acl    = "private"
    lifecycle_rule = {
        abort_incomplete_multipart_upload_days = "7"
        enabled = "true"
        expiration = {
            days = "1"
            expired_object_delete_marker = "false"
        }
        noncurrent_version_expiration = {
            days = "1"
        }
    }
    logging = {
        target_bucket = "dynamite-gigya-export-access-logs"
        target_prefix = "" 
    }
}

resource "aws_s3_bucket" "dynamite-gigya-export-access-logs" {
    bucket = "dynamite-gigya-export-access-logs"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-google" {
    bucket = "dynamite-google"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-infra-terraform-state-bucket" {
    bucket = "dynamite-infra-terraform-state-bucket"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-oracle-installers" {
    bucket = "dynamite-oracle-installers"
    acl    = "private"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "Policy1497300048447",
  "Statement": [
    {
      "Sid": "Dev_infra_access",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::118421851785:root"
      },
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::dynamite-oracle-installers/*"
    },
    {
      "Sid": "Dev_infra_access",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::118421851785:root"
      },
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::dynamite-oracle-installers"
    }
  ]
}
POLICY
}

resource "aws_s3_bucket" "dynamite-share-home" {
    bucket = "dynamite-share-home"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-temp" {
    bucket = "dynamite-temp"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-vmimports" {
    bucket = "dynamite-vmimports"
    acl    = "private"
}

resource "aws_s3_bucket" "dynamite-yum-packages" {
    bucket = "dynamite-yum-packages"
    acl    = "private"
}

resource "aws_s3_bucket" "gdi-atg-db-dumps" {
    bucket = "gdi-atg-db-dumps"
    acl    = "private"
}

resource "aws_s3_bucket" "sms-b-us-east-1-sqkz4nxkaq3erobpjz7esngjtk7f3t2d" {
    bucket = "sms-b-us-east-1-sqkz4nxkaq3erobpjz7esngjtk7f3t2d"
    acl    = "private"
    lifecycle_rule = {
        abort_incomplete_multipart_upload_days = "14"
        enabled = "true"
        expiration = {
            days = "14"
            expired_object_delete_marker = "false"
        }
    }
}

resource "aws_s3_bucket" "vx9000b" {
    bucket = "vx9000b"
    acl    = "private"
}

