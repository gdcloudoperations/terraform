resource "aws_instance" "PAWDCP01" {
    ami                         = "ami-085ea1972627f58fd"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.82"
    source_dest_check           = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 80
        delete_on_termination = true
    }

    tags {
        "Name" = "PAWDCP01"
        "Application" = "AD SVR"
        "GDType" = "AD"
        "Retention" = "2"
        "Backup" = "Yes"
        "Owner" = "Server Support"
        "Project" = "Infra"
    }
}
resource "aws_instance" "SIADCW03" {
    ami                         = "ami-041114ddee4a98333"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "m4.large"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.10"
    source_dest_check           = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 80
        delete_on_termination = true
    }

    tags {
        "Name" = "SIADCW03"
        "Application" = "DC for Corp.gdglobal.ca"
        "GDType" = "AD"
        "Retention" = "2"
        "Backup" = "Yes"
        "Owner" = "Server Support"
        "Project" = "Infra"
    }
}
resource "aws_instance" "SIADCH02---AD" {
    ami                         = "ami-9046b8ea"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = ""
    subnet_id                   = "subnet-56e1700c"
    vpc_security_group_ids      = ["sg-5e12bc2f"]
    associate_public_ip_address = false
    private_ip                  = "172.30.50.202"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 60
        delete_on_termination = false
    }

    tags {
        "Name" = "SIADCH02   AD"
    }
}
resource "aws_instance" "SIADCH03--AD" {
    ami                         = "ami-ad44bad7"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = ""
    subnet_id                   = "subnet-56e1700c"
    vpc_security_group_ids      = ["sg-5e12bc2f"]
    associate_public_ip_address = false
    private_ip                  = "172.30.50.207"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = false
    }

    tags {
        "Name" = "SIADCH03  AD"
    }
}
resource "aws_instance" "SIADCH04--AD" {
    ami                         = "ami-bbb946c1"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = ""
    subnet_id                   = "subnet-56e1700c"
    vpc_security_group_ids      = ["sg-5e12bc2f"]
    associate_public_ip_address = false
    private_ip                  = "172.30.50.208"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = false
    }

    tags {
        "Owner" = ""
        "Name" = "SIADCH04  AD"
    }
}
resource "aws_instance" "PAWAPADFS01" {
    ami                         = "ami-0f4c7e570f044b46f"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    private_ip                  = "172.28.1.159"
    associate_public_ip_address = false
    source_dest_check           = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 80
        delete_on_termination = false
    }

    tags {
        "Name" = "PAWAPADFS01"
    }
}

resource "aws_instance" "PAWAPADFS02" {
    ami                         = "ami-0f4c7e570f044b46f"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    private_ip                  = "172.28.1.170"
    associate_public_ip_address = false
    source_dest_check           = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 80
        delete_on_termination = false
    }

    tags {
        "Name" = "PAWAPADFS02"
    }
}
resource "aws_instance" "PAWAPADFSP02" {
    ami                         = "ami-07a29e78aeb420471"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-ea4057b1"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    source_dest_check           = true
    iam_instance_profile        = "Infra"
    private_ip                  = "172.28.101.35"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 80
        delete_on_termination = true
    }

    tags {
        "Name" = "PAWAPADFSP02"
    }
}
resource "aws_instance" "PAWAPADFSP01" {
    ami                         = "ami-07a29e78aeb420471"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-ea4057b1"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    source_dest_check           = true
    iam_instance_profile        = "Infra"
    private_ip                  = "172.28.101.53"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 80
        delete_on_termination = true
    }

    tags {
        "Name" = "PAWAPADFSP01"
    }
}