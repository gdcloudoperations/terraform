resource "aws_instance" "saobih81" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.large"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.55"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = false
    }

    tags {
        "Application" = "ORACLE APP13 TST"
        "Retention" = "2"
        "Backup" = "Yes"
        "Name" = "saobih81"
        "Project" = "RMS13 TEST"
        "ScheduleType" = "MIGRATE"
        "Owner" = "RMS"
        "GDType" = "TERRAFORM"
    }
}
resource "aws_instance" "samomw93" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.41"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    tags {
        "ScheduleType" = "DEV"
        "Backup" = "Yes"
        "Name" = "samomw93"
        "Retention" = "2"
        "Project" = "RMS16 APP DEV"
        "Owner" = "RMS"
        "Application" = "ORACLE APP16 DEV"
        "GDType" = "TERRAFORM"
    }
}
resource "aws_instance" "dawaprib02" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.143"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = true
    }

    tags {
        "Retention" = "2"
        "Backup" = "Yes"
        "Name" = "dawaprib02"
        "ScheduleType" = "DEV"
        "GDType" = "TERRAFORM"
        "Application" = "ORACLE APP13 T02"
        "Owner" = "RMS"
        "Project" = "RMS13 T02"
    }
}
resource "aws_instance" "sasoah81" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.large"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.53"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = false
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = false
    }

    tags {
        "Project" = "RMS13 TEST"
        "Application" = "ORACLE APP13 TST"
        "Name" = "sasoah81"
        "Owner" = "RMS"
        "GDType" = "TERRAFORM"
        "ScheduleType" = "MIGRATE"
        "Backup" = "Yes"
        "Retention" = "2"
    }
}
resource "aws_instance" "dawaprms02" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.141"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = true
    }

    tags {
        "Name" = "dawaprms02"
        "Owner" = "RMS"
        "ScheduleType" = "DEV"
        "Backup" = "Yes"
        "Application" = "ORACLE APP13 T02"
        "Retention" = "2"
        "Project" = "RMS13 T02"
        "GDType" = "TERRAFORM"
    }
}
resource "aws_instance" "samomw91" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.40"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    tags {
        "Retention" = "2"
        "Backup" = "Yes"
        "Application" = "ORACLE APP16 GOLD"
        "Project" = "RMS16 APP GOLD"
        "Owner" = "RMS"
        "Name" = "samomw91"
        "ScheduleType" = "DEV"
        "GDType" = "TERRAFORM"
    }
}
resource "aws_instance" "satstt99" {
    ami                         = "ami-0f8d02d6ce1d7bbae"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.large"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.210"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    ebs_block_device {
        device_name           = "/dev/sdg"
        snapshot_id           = "snap-06a53808e05ca740d"
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = false
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/sdi"
        snapshot_id           = "snap-0244328cd75c126ad"
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/sdf"
        snapshot_id           = "snap-0787468eaae716d61"
        volume_type           = "gp2"
        volume_size           = 16
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/sdh"
        snapshot_id           = "snap-05c30f9d5a3338fcd"
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    tags {
        "Owner" = "RMS"
        "Name" = "satstt99"
        "Project" = "OVS TESTONLY"
        "GDType" = "IMPORT_OVS"
    }
}
resource "aws_instance" "dawapbat02" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.140"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = true
    }

    tags {
        "Owner" = "RMS"
        "ScheduleType" = "DEV"
        "Project" = "RMS13 T02"
        "Backup" = "Yes"
        "Retention" = "2"
        "GDType" = "TERRAFORM"
        "Application" = "ORACLE APP13 T02"
        "Name" = "dawapbat02"
    }
}
resource "aws_instance" "sastoh81" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.large"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.54"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = false
    }

    tags {
        "Owner" = "RMS"
        "ScheduleType" = "MIGRATE"
        "GDType" = "TERRAFORM"
        "Application" = "ORACLE APP13 TST"
        "Backup" = "Yes"
        "Name" = "sastoh81"
        "Retention" = "2"
        "Project" = "RMS13 TEST"
    }
}
resource "aws_instance" "sdoraw92" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.100"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 16
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 500
        delete_on_termination = false
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    tags {
        "ScheduleType" = "DEV16"
        "Name" = "sdoraw92"
        "Project" = "RMS16 DB"
        "Retention" = "2"
        "Backup" = "Yes"
        "Application" = "Oracle DB rms16"
        "GDType" = "TERRAFORM"
        "Owner" = "RMS"
    }
}
resource "aws_instance" "sarmsh81" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.large"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.56"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    tags {
        "ScheduleType" = "MIGRATE"
        "Owner" = "RMS"
        "GDType" = "TERRAFORM"
        "Application" = "ORACLE APP13 TST"
        "Retention" = "2"
        "Project" = "RMS13 TEST"
        "Name" = "sarmsh81"
        "Backup" = "Yes"
    }
}
resource "aws_instance" "dawdbora01" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.101"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 16000
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdf"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 2000
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 200
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 16
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdg"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10000
        delete_on_termination = false
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    tags {
        "Project" = "RMS13 DB"
        "GDType" = "TERRAFORM"
        "Application" = "Oracle DB DVLP"
        "Retention" = "2"
        "Backup" = "No"
        "Name" = "dawdbora01"
        "Owner" = "RMS"
        "ScheduleType" = "MIGRATE"
    }
}
resource "aws_instance" "sabath81" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.large"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.51"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 300
        delete_on_termination = false
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = false
    }

    tags {
        "ScheduleType" = "MIGRATE"
        "Name" = "sabath81"
        "Retention" = "2"
        "Application" = "ORACLE APP13 TST"
        "Backup" = "Yes"
        "GDType" = "TERRAFORM"
        "Owner" = "RMS"
        "Project" = "RMS13 TEST"
    }
}
resource "aws_instance" "dawapobi02" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.146"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = true
    }

    tags {
        "Backup" = "Yes"
        "Retention" = "2"
        "Application" = "ORACLE APP13 T02"
        "Project" = "RMS13 T02"
        "ScheduleType" = "DEV"
        "Name" = "dawapobi02"
        "Owner" = "RMS"
        "GDType" = "TERRAFORM"
    }
}
resource "aws_instance" "dawaprco02" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.144"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = true
    }

    tags {
        "Backup" = "Yes"
        "Retention" = "2"
        "Application" = "ORACLE APP13 T02"
        "Name" = "dawaprco02"
        "ScheduleType" = "DEV"
        "GDType" = "TERRAFORM"
        "Owner" = "RMS"
        "Project" = "RMS13 T02"
    }
}
resource "aws_instance" "dawapodi02" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.149"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = true
    }

    tags {
        "Owner" = "RMS"
        "Name" = "dawapodi02"
        "Project" = "RMS13 T02"
        "GDType" = "TERRAFORM"
        "Backup" = "Yes"
        "Application" = "ORACLE APP13 T02"
        "ScheduleType" = "DEV"
        "Retention" = "2"
    }
}
resource "aws_instance" "dawapsoa02" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-2575745a"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.148"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 10
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 5
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = true
    }

    tags {
        "GDType" = "TERRAFORM"
        "Name" = "dawapsoa02"
        "Owner" = "RMS"
        "Project" = "RMS13 T02"
        "Backup" = "Yes"
        "Application" = "ORACLE APP13 T02"
        "ScheduleType" = "DEV"
        "Retention" = "2"
    }
}