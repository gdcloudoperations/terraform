
############ PRODUCTION ENVIRONMENT ############
resource "aws_instance" "PAWRDSWMS01" {
    ami                         = "ami-a2bd89dd"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "r5.xlarge"
    monitoring                  = true
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.228"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 120
        delete_on_termination = true
    }

    tags {
        "Name" = "PAWRDSWMS01"
        "Backup" = "Yes"
        "Application" = "WMS RF RDP servers"
        "Owner" = "Brian Gonsalves"
        "Project" = "WMS"
        "ScheduleType" = ""
    }
}
resource "aws_instance" "PAWAPWMS01" {
    ami                         = "ami-07709b2e6300dab3e"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.xlarge"
    monitoring                  = true
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.104"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    ebs_block_device {
        device_name           = "/dev/sdf"
        snapshot_id           = "snap-047767a33cfdd7e2b"
        volume_type           = "gp2"
        volume_size           = 80
        delete_on_termination = false
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 150
        delete_on_termination = false
    }

    tags {
        "Owner" = "Brian Gonsalves"
        "ScheduleType" = ""
        "Application" = "WMS Master & RDP for Remote App users"
        "Project" = "WMS"
        "Backup" = "Yes"
        "Name" = "PAWAPWMS01"
    }
}
resource "aws_instance" "PAWRDSWMS02" {
    ami                         = "ami-a2bd89dd"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "r5.large"
    monitoring                  = true
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.71"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 130
        delete_on_termination = true
    }

    tags {
        "ScheduleType" = ""
        "Name" = "PAWRDSWMS02"
        "Application" = "WMS RF RDP servers"
        "Project" = "WMS"
        "Owner" = "Brian Gonsalves"
    }
}
resource "aws_instance" "PAWRDSWMS03" {
    ami                         = "ami-000f495116b0cf553"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "r5.large"
    monitoring                  = true
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.22"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    tags {
        "ScheduleType" = ""
        "Project" = "WMS"
        "Application" = "WMS RF RDP servers"
        "Name" = "PAWRDSWMS03"
        "Owner" = "Brian Gonsalves"
    }
}
resource "aws_instance" "PAWRDSWMS04" {
    ami                         = "ami-000f495116b0cf553"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "r5.large"
    monitoring                  = true
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.236"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    tags {
        "Application" = "WMS RF RDP servers"
        "Project" = "WMS"
        "ScheduleType" = ""
        "Owner" = "Brian Gonsalves"
        "Name" = "PAWRDSWMS04"
    }
}
resource "aws_instance" "PAWIISWMS01" {
    ami                         = "ami-b45e1dcb"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = true
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.188"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    tags {
        "Name" = "PAWIISWMS01"
        "Backup" = "Yes"
        "ScheduleType" = ""
        "Owner" = "Brian Gonsalves"
        "Application" = "WMS IIS servers"
        "Project" = "WMS"
    }
}
resource "aws_instance" "PAWIISWMS02" {
    ami                         = "ami-b45e1dcb"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = true
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.152"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    tags {
        "Application" = "WMS IIS servers"
        "Project" = "WMS"
        "Backup" = "Yes"
        "ScheduleType" = ""
        "Name" = "PAWIISWMS02"
        "Owner" = "Brian Gonsalves"
    }
}


############ DEVELOPMENT ENVIRONMENT ############
resource "aws_instance" "SAWMSW92" {
    ami                         = "ami-638ac91c"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = true
    key_name                    = ""
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.58"
    source_dest_check           = true
    disable_api_termination     = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 30
        delete_on_termination = true
    }

    tags {
        "Owner" = "Brian Gonsalves"
        "Project" = "WMS"
        "Name" = "SAWMSW92"
        "Application" = "WMS - RDP RF"
        "GDType" = "WMSDEV"
        "Backup" = "Yes"
        "Retention" = "2"
        "ScheduleType" = "DEV"
    }
}
resource "aws_instance" "SAWMSW93" {
    ami                         = "ami-327c3f4d"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = true
    key_name                    = ""
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.105"
    source_dest_check           = true
    disable_api_termination     = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    tags {
        "Owner" = "Brian Gonsalves"
        "Retention" = "2"
        "Backup" = "Yes"
        "Application" = "WMS - IIS Server"
        "ScheduleType" = "DEV"
        "GDType" = "WMSDEV"
        "Name" = "SAWMSW93"
        "Project" = "WMS"
    }
}
resource "aws_instance" "SAWMSW94" {
    ami                         = "ami-327c3f4d"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = true
    key_name                    = ""
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.79"
    source_dest_check           = true
    disable_api_termination     = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    tags {
        "Name" = "SAWMSW94"
        "Owner" = "Brian Gonsalves"
        "Project" = "WMS"
        "Application" = "WMS - IIS Server"
        "ScheduleType" = "DEV"
        "Retention" = "2"
        "GDType" = "WMSDEV"
        "Backup" = "Yes"
    }
}
resource "aws_instance" "SAWMSW97" {
    ami                         = "ami-b45e1dcb"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.xlarge"
    monitoring                  = true
    key_name                    = ""
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.85"
    source_dest_check           = true
    disable_api_termination     = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/sdf"
        snapshot_id           = "snap-08f4bb65bda5f5f70"
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = false
    }

    tags {
        "Application" = "WMS Master"
        "Owner" = "Brian Gonsalves"
        "Retention" = "2"
        "Backup" = "Yes"
        "GDType" = "WMSDEV"
        "ScheduleType" = "DEV"
        "Project" = "WMS"
        "Name" = "SAWMSW97"
    }
}

resource "aws_instance" "DAWAPWMS02" {
    ami                         = "ami-05d2216b5dfd8a931"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.large"
    monitoring                  = true
    key_name                    = ""
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.121"
    source_dest_check           = true
    disable_api_termination     = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/sdf"
        snapshot_id           = "snap-04aa30bd25abd984f"
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = false
    }

    tags {
        "Application" = "TEST WMS Master APP for Omni"
        "Owner" = "Brian Gonsalves"
        "GDType" = "WMSDEV"
        "ScheduleType" = "DEV"
        "Project" = "WMS"
        "Name" = "DAWAPWMS02"
    }
}

resource "aws_instance" "DAWAPWMS03" {
    ami                         = "ami-0272543440d41f1ea"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.xlarge"
    monitoring                  = true
    key_name                    = ""
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    #private_ip                  = "172.28.1.85"
    source_dest_check           = true
    disable_api_termination     = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/sdf"
        snapshot_id           = "snap-032d7d972b9750f49"
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = true
    }

    tags {
        "Application" = "WMS Master"
        "Owner" = "Brian Gonsalves"
        "Retention" = "2"
        "GDType" = "WMSDEV"
        "scheduleTypeUTC" = "OMNI"
        "Project" = "WMS"
        "Name" = "DAWAPWMS03"
    }
}

