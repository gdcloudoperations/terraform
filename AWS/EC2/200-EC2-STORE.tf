resource "aws_instance" "DAWXLRDB02" {
    ami                         = "ami-2d360152"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.large"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.78"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    tags {
        "Name" = "DAWXLRDB02"
        "Project" = "XSTOR"
        "Application" = "Xstore Lead Register"
        "Owner" = "Romuald Gain"
        "ScheduleType" = ""
    }
}
resource "aws_instance" "DAWXLRDB01" {
    ami                         = "ami-2d360152"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.large"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.98"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    tags {
        "Owner" = "Romuald Gain"
        "Application" = "Xstore Lead Register"
        "Project" = "XSTOR"
        "Name" = "DAWXLRDB01"
        "ScheduleType" = ""
    }
}
resource "aws_instance" "DAWXNLR01" {
    ami                         = "ami-2d360152"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.37"
    source_dest_check           = true
    iam_instance_profile        = "Infra"

    ebs_block_device {
        device_name           = "xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = true
    }

    tags {
        "Project" = "XSTOR"
        "ScheduleType" = ""
        "Application" = "Xstore Non-Lead Register"
        "Name" = "DAWXNLR01"
        "Owner" = "Romuald Gain"
    }
}
resource "aws_instance" "DAWXNLR02" {
    ami                         = "ami-2d360152"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.45"
    source_dest_check           = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    tags {
        "Name" = "DAWXNLR02"
        "Owner" = "Romuald Gain"
        "Application" = "Xstore Non-Lead Register"
        "Project" = "XSTOR"
        "ScheduleType" = ""
    }
}
resource "aws_instance" "DAWXCSDB01" {
    ami                         = "ami-8a86b2f5"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m5.xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.57"
    source_dest_check           = true
    disable_api_termination     = true
    iam_instance_profile        = "Infra"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 80
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 200
        delete_on_termination = true
    }

    tags {
        "Name" = "DAWXCSDB01"
        "Backup" = "Yes"
        "ScheduleType" = ""
        "Application" = "Xcenter Server for DB, MW, App"
        "Project" = "XSTOR"
        "Owner" = "Romuald Gain"
        "Retention" = "2"
    }
}