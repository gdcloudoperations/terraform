resource "aws_instance" "RUTMW01" {
    ami                         = "ami-97f7d480"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "m3.medium"
    monitoring                  = false
    key_name                    = ""
    subnet_id                   = "subnet-ea4057b1"
    vpc_security_group_ids      = ["sg-34691b4a"]
    associate_public_ip_address = true
    private_ip                  = "172.28.101.240"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "/dev/sdb"
        snapshot_id           = ""
        volume_type           = "standard"
        volume_size           = 60
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = true
    }

    tags {
        "Application" = "ENT FRWL"
        "Retention" = "2"
        "Backup" = "Yes"
        "Owner" = "Server Support"
        "Public IP" = "172.28.101.240"
        "Private IP" = "172.28.1.6"
        "Project" = "Infra"
        "GDType" = "Fortigate"
        "Name" = "RUTMW01"
    }
}
resource "aws_instance" "SIWLSW01" {
    ami                         = "ami-a55965df"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-ee758299"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.251"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    ebs_block_device {
        device_name           = "/dev/sdg"
        snapshot_id           = "snap-0d8dffb8db8466984"
        volume_type           = "gp2"
        volume_size           = 85
        delete_on_termination = false
    }

    tags {
        "GDType" = "VX9000"
        "Application" = "WLS CTL"
        "Name" = "SIWLSW01"
        "Private IP" = "172.28.1.251"
        "Project" = "Infra"
        "Retention" = "2"
        "Owner" = "Server Support"
        "Backup" = "Yes"
    }
}
resource "aws_instance" "LIBRESWAN" {
    ami                         = "ami-9887c6e7"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.micro"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-ea4057b1"
    vpc_security_group_ids      = ["sg-030b59797f9b3114f"]
    associate_public_ip_address = false
    private_ip                  = "172.28.101.245"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = false
    }

    tags {
        "Application" = "LIBRESWAN"
        "Project" = "Infra"
        "Owner" = "Server Support"
        "Name" = "LIBRESWAN"
    }
}
resource "aws_instance" "SNRADW01" {
    ami                         = "ami-1c4f1c66"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.15"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    tags {
        "GDType" = "RADIUS"
        "Name" = "SNRADW01"
        "Owner" = "Server Support"
        "Application" = "RADIUS"
        "Backup" = "Yes"
        "Retention" = "2"
        "Project" = "Infra"
    }
}