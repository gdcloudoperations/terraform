


resource "aws_instance" "OEL74_TEMPLACE" {
    ami                         = "ami-79227506"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-9c2482ec"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.20"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 25
        delete_on_termination = false
    }

    tags {
        "Backup" = "Yes"
        "Retention" = "1"
        "GDType" = "TEMPLATE"
        "Name" = "OEL74_TEMPLACE"
    }
}
resource "aws_instance" "SQL-TEMPLATE" {
    ami                         = "ami-f6529b8c"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.2xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-9c2482ec"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.52"
    source_dest_check           = true
    disable_api_termination     = true

    ebs_block_device {
        device_name           = "xvdc"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 2000
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvde"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 200
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvdd"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 200
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 200
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 1
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvdf"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 500
        delete_on_termination = true
    }

    tags {
        "Name" = "SQL TEMPLATE"
    }
}
resource "aws_instance" "i-01cd24278dcf0dda1" {
    ami                         = "ami-0f9cf087c1f27d9b1"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.micro"
    monitoring                  = false
    key_name                    = ""
    subnet_id                   = "subnet-ea4057b1"
    vpc_security_group_ids      = ["sg-1f5f1e60"]
    associate_public_ip_address = false
    private_ip                  = "172.28.101.238"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 8
        delete_on_termination = true
    }

    tags {
        "test" = "coucou"
    }
}


