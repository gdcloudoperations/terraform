/* A "backend" in Terraform determines how state is
   loaded and how an operation such as apply is executed.
   This abstraction enables non-local file state storage,
   remote execution, etc.

   https://www.terraform.io/docs/backends/types/s3.html
*/
terraform {
  backend "s3" {
    bucket  = "dynamite-infra-terraform-state-bucket"
    key	    = "prod/terraform/AWS/EC2"
    region  = "us-east-1"
  }
}
