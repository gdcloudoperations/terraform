resource "aws_instance" "SACOMH01--Commvault" {
    ami                         = "ami-0beffc1d"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "c4.4xlarge"
    monitoring                  = false
    key_name                    = ""
    subnet_id                   = "subnet-56e1700c"
    vpc_security_group_ids      = ["sg-5e12bc2f"]
    associate_public_ip_address = false
    private_ip                  = "172.30.50.60"
    source_dest_check           = true

    ebs_block_device {
        device_name           = "xvdb"
        snapshot_id           = "snap-0b4d669377287173d"
        volume_type           = "gp2"
        volume_size           = 60
        delete_on_termination = false
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 70
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvdc"
        snapshot_id           = "snap-0641787dbb0b3739e"
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = false
    }

    tags {
        "GDLogonTested" = "true"
        "Name" = "SACOMH01  Commvault"
        "GDType" = "CV-MediaAgent-Cold"
    }
}
resource "aws_instance" "SACOMT02-Commvault" {
    ami                         = "ami-f6529b8c"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "m4.2xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-a3f061f9"
    vpc_security_group_ids      = ["sg-2316b852"]
    associate_public_ip_address = false
    private_ip                  = "172.31.50.22"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    tags {
        "GDType" = "CV-MediaAgent"
        "Name" = "SACOMT02 Commvault"
    }
}
resource "aws_instance" "SACOMT01--Commvault" {
    ami                         = "ami-56438a2c"
    availability_zone           = "us-east-1c"
    ebs_optimized               = true
    instance_type               = "c4.xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-a3f061f9"
    vpc_security_group_ids      = ["sg-2316b852"]
    associate_public_ip_address = false
    private_ip                  = "172.31.50.185"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    tags {
        "GDType" = "CV-MediaAgent"
        "Name" = "SACOMT01  Commvault"
        "Project" = "DR"
    }
}