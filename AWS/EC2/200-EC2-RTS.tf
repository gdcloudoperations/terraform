//resource "aws_instance" "PAWAPRTS01" {
//    ami = "ami-0a14d0f7accd6872a"
//    associate_public_ip_address = "false"
//    availability_zone = "us-east-1c"
//    cpu_core_count = "2"
//    cpu_threads_per_core = "1"
//    credit_specification = {
//        cpu_credits = "standard"
//    }
//    disable_api_termination = "false"
//    ebs_block_device = {
//        delete_on_termination = "true"
//        device_name = "/dev/sdf"
//        encrypted = "false"
//        iops = "603"
//        snapshot_id = "snap-015e81504e797217d"
//        volume_size = "201"
//        volume_type = "gp2"
//    }
//    ebs_optimized = "false"
//    get_password_data = "false"
//    iam_instance_profile = "Infra"
//    instance_type = "t2.medium"
//    key_name = "shared-gd"
//    monitoring = "false"
//    placement_group = ""
//    private_ip = "172.28.1.123"
//    root_block_device = {
//        delete_on_termination = "false"
//        iops = "243"
//        volume_size = "81"
//        volume_type = "gp2"
//    }
//    source_dest_check = "true"
//    subnet_id = "subnet-4d455216"
//    tags = {
//        OldName = "SAPTHH07"
//        Server = "RTS"
//        ScheduleType = "DEV"
//        Name = "DAWAPRTS01"
//        Description = "Authorization service for PINPADs"
//    }
//    tenancy = "default"
//    vpc_security_group_ids = ["sg-b93214c6"]
//}
