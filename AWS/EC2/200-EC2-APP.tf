resource "aws_instance" "DAWAPMID01" {
    ami                         = "ami-0dbe1f3a50277541f"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.large"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.133"
    source_dest_check           = true
    iam_instance_profile        = "Infra"

    ebs_block_device {
        device_name           = "/dev/sdf"
        snapshot_id           = "snap-061c98c18a215cd75"
        volume_type           = "gp2"
        volume_size           = 50
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 60
        delete_on_termination = true
    }

    tags {
        "ScheduleType" = "DEV"
        "Application" = "MID APP TEST"
        "Name" = "DAWAPMID01"
        "Owner" = "Ilham"
    }
}
resource "aws_instance" "DAWSQLTS01" {
    ami = "ami-0e32926c50c7761b9"
    associate_public_ip_address = "false"
    availability_zone = "us-east-1c"
    cpu_core_count = "1"
    cpu_threads_per_core = "2"
    credit_specification = {
        cpu_credits = "standard"
    }
    disable_api_termination = "true"
    ebs_optimized = "true"
    iam_instance_profile = "Infra"
    instance_type = "m4.large"
    key_name = "shared-gd"
    monitoring = "true"
    private_ip = "172.28.1.67"
    root_block_device = {
        delete_on_termination = "false"
        iops = "600"
        volume_size = "220"
        volume_type = "gp2"
    }
    source_dest_check = "true"
    subnet_id = "subnet-4d455216"
    tags = {
        Application = "Tradestone database server"
        Backup = ""
        Name = "DAWSQLTS01"
        Owner = "Francis Caron"
        Project = "Tradestone"
        ScheduleType = "DEV"
    }
    tenancy = "default"
    vpc_security_group_ids =["sg-b93214c6"]
}

resource "aws_instance" "PAWAPCLX01" {
    ami                         = "ami-0d7cc82871a4f671a"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.96"
    source_dest_check           = true
    iam_instance_profile        = "Infra"
    
    root_block_device = {
        volume_type           = "gp2"
        volume_size           = 80
        delete_on_termination = true
    }

    

   
    tags {
        "ScheduleType" = "DEV"
        "Application" = "CLX APP PROD"
        "Name" = "PAWAPCLX01"
        "Owner" = "SYS Administrator"
        "Backup" = "Yes"
    }
}
 resource "aws_instance" "PAWAPCLX02" {
    ami                         = "ami-08099473475295e16"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.medium"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.132"
    source_dest_check           = true
    iam_instance_profile        = "Infra"
    
    root_block_device = {
        
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }

    ebs_block_device = {   
        device_name           = "/dev/sdf"
        snapshot_id           = "snap-0c718a8acf3337d77"
        volume_type           = "gp2"
        volume_size           = 40
        delete_on_termination = true
    }


    tags {
        "ScheduleType" = "DEV"
        "Application" = "CLX APP PROD"
        "Name" = "PAWAPCLX02"
        "Owner" = "SYS Administrator"
        "Backup" = "Yes"
    }
}
resource "aws_instance" "PAWAPIJ01" {
    ami                         = "ami-0df43b4f8a07c7c14"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "t2.small"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-4d455216"
    vpc_security_group_ids      = ["sg-b93214c6"]
    associate_public_ip_address = false
    private_ip                  = "172.28.1.196"
    source_dest_check           = true
    iam_instance_profile        = "Infra"
    
    root_block_device = {
        volume_type           = "gp2"
        volume_size           = 80
        delete_on_termination = true
    }

    

   
    tags {
        "Application" = "integrate Jenkins"
        "Name" = "PAWAPIJ01"
        "Owner" = "Abul-Kalam Ullah"
        "Backup" = "Yes"
    }
}