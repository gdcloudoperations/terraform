resource "aws_instance" "SACOMT08" {
    ami                         = "ami-d87512a2"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "m4.2xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-a3f061f9"
    vpc_security_group_ids      = ["sg-2316b852"]
    associate_public_ip_address = false
    private_ip                  = "172.31.50.197"
    source_dest_check           = true
    iam_instance_profile        = "s3-readonly"

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 60
        delete_on_termination = true
    }

    tags {
        "Name" = "SACOMT08"
        "GDType" = "CV-MediaAgent"
    }
}
resource "aws_instance" "SACOMT06" {
    ami                         = "ami-d87512a2"
    availability_zone           = "us-east-1c"
    ebs_optimized               = false
    instance_type               = "m4.2xlarge"
    monitoring                  = false
    key_name                    = "shared-gd"
    subnet_id                   = "subnet-a3f061f9"
    vpc_security_group_ids      = ["sg-2316b852"]
    associate_public_ip_address = false
    private_ip                  = "172.31.50.189"
    source_dest_check           = true
    iam_instance_profile        = "s3-readonly"

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 60
        delete_on_termination = true
    }

    ebs_block_device {
        device_name           = "/dev/xvdb"
        snapshot_id           = ""
        volume_type           = "gp2"
        volume_size           = 100
        delete_on_termination = true
    }

    tags {
        "Name" = "SACOMT06"
        "GDType" = "CV-MediaAgent"
    }
}