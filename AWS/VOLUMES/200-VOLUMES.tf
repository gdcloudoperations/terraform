resource "aws_ebs_volume" "vol-0f10f25e62766f745" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "10000"
    snapshot_id = ""
    tags = {
        SERVER = "dawdbora01"
    }    
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0ca51ad2556bb9990" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "80"
    snapshot_id = "snap-01f3e58dc374ed830"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-01e369001f5e6cc8c" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "80"
    snapshot_id = "snap-09151d99a7b10f3da"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0cdaf73c5f3cd03d9" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "100"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0951f8693bff51d0b" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "60"
    snapshot_id = "snap-091d64c19ed123dc0"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-038ff13c2a92f3062" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "10"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0d0b4e07e06c6b91f" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "25"
    snapshot_id = "snap-01725e1159b37f7d1"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0496ed6cfaf8de352" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "5"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0081adf9cd8ed1d96" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "40"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0870a7b876b01a1e0" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "8"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-014c312c8891ed05a" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "10"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0a670f8f54c17cf56" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "25"
    snapshot_id = "snap-01725e1159b37f7d1"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0fd6c88b51adfbef5" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "5"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-07a52a542f8163d0f" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "40"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0cb73f2e20e3c86b9" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "8"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-01f9820348c657a11" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "10"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0e43564578a4746a1" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "25"
    snapshot_id = "snap-01725e1159b37f7d1"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0472e28711b820a84" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "5"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-020b2c23e129dd8f1" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "40"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0bfa16bbdcd3ad7f1" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "8"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-05fd02ca892523773" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "50"
    snapshot_id = "snap-061c98c18a215cd75"
    tags = {
        Owner = "Ilham"
        ScheduleType = "DEV"
    }
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0b48ac20c9284927e" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "60"
    snapshot_id = "snap-0b5ecc57c278762af"
    tags = {
        Owner = "Ilham"
        ScheduleType = "DEV"
    }
    type = "gp2"
}
resource "aws_ebs_volume" "vol-03721cbb7141d387c" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "16000"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0efccc5a9b21ca188" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "2000"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0397dccff05b32b0b" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "25"
    snapshot_id = "snap-01725e1159b37f7d1"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-09001a64191d826ff" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "10"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0affb850b875c9055" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "200"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0ccc7b73e13a5b7d7" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "16"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-06a569cbc6638d4d8" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "10000"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-01a6abec5a3d01726" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "2000"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0352c2fae7e899542" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "25"
    snapshot_id = "snap-01725e1159b37f7d1"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-093076be48f791ddd" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "10"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0c036eb723da38f22" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "200"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-06b115b21c0909576" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "16"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-099d8de083392c557" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "8"
    snapshot_id = "snap-0d6430e51c0056751"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-04b4b9cc8e92a3ae2" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "10000"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-085fd58c9a4e0b86b" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "25"
    snapshot_id = "snap-01725e1159b37f7d1"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0c57950d8b60ac7a5" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "10"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0797506526ff79fe8" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "200"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0c12c53d8f2ade49e" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "16"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-037749acb8c699876" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "50"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0d2789901235a5954" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "25"
    snapshot_id = "snap-01725e1159b37f7d1"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-07fd1218927e17e06" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "5"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-05f7e7a79796e4c98" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "100"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0fa4c0fab9ee01210" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "8"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-089059ea9d3c7cbac" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "50"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-031d73e3ad20ef16d" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "50"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0e91e0dddfadfed9c" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "25"
    snapshot_id = "snap-01725e1159b37f7d1"
    type = "gp2"
}
resource "aws_ebs_volume" "vol-01f711c2f062dacce" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "100"
    snapshot_id = ""
    type = "gp2"
}
resource "aws_ebs_volume" "vol-0efc06d1b3e7749fa" {
    availability_zone = "us-east-1c"
    encrypted = "false"
    size = "25"
    snapshot_id = "snap-01725e1159b37f7d1"
    type = "gp2"
}