resource "azurerm_app_service" "GdiWebApp1" {
  app_settings = {
    MobileAppsManagement_EXTENSION_VERSION = "latest"
    WEBSITE_NODE_DEFAULT_VERSION = "6.9.1"
  }
  app_service_plan_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/NewResourceGroup/providers/Microsoft.Web/serverfarms/ServicePlan40b415ae-b7dc"
  client_affinity_enabled = "true"
  connection_string = {
    name = "defaultConnection"
    type = "SQLAzure"
    value = "Data Source=tcp:biprod01.database.windows.net,1433;Initial Catalog=BiCenter;User Id=null@biprod01.database.windows.net;Password=Montreal00!;"
  }
  enabled = "true"
  https_only = "false"
  location = "centralus"
  name = "GdiWebApp1"
  resource_group_name = "NewResourceGroup"
  site_config{
    always_on = "false"
    default_documents = [
      "Default.htm"
      ,"Default.html"
      ,"Default.asp"
      ,"index.htm"
      ,"index.html"
      ,"iisstart.htm"
      ,"default.aspx"
      ,"index.php"
      ,"hostingstart.html"
    ]
    dotnet_framework_version = "v4.0"
    ftps_state = "AllAllowed"
    http2_enabled = "false"
    local_mysql_enabled = "false"
    managed_pipeline_mode = "Integrated"
    min_tls_version = "1.0"
    php_version = "7.2"
    remote_debugging_enabled = "false"
    scm_type = "None"
    use_32_bit_worker_process = "true"
    virtual_network_name = ""
    websockets_enabled = "false"
  }
}