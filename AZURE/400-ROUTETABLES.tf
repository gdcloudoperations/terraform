resource "azurerm_route_table" "ForcedTunelling" {
    disable_bgp_route_propagation = "false"
    location = "canadaeast"
    name = "ForcedTunelling"
    resource_group_name = "Gdi-ResGrpBi02"
}