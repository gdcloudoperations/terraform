resource "azurerm_virtual_machine" "sabipz02" {
    boot_diagnostics = {
        enabled = true
        storage_uri = "https://gdiresgrpbi02diag741.blob.core.windows.net/"
    }
    location = "canadaeast"
    name = "sabipz02"
    network_interface_ids = [
        "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/Gdi-ResGrpBi02/providers/Microsoft.Network/networkInterfaces/sabipz02715"
    ]
    resource_group_name = "Gdi-ResGrpBi02"
    storage_data_disk = [
        {
            caching = "ReadWrite"
            create_option = "Attach"
            disk_size_gb = "1023"
            lun = "0"
            managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/GDI-RESGRPBI02/providers/Microsoft.Compute/disks/GDI-BiProd02_disk2_0e9c12ec436e4198bd455a75396401fe"
            managed_disk_type = "Premium_LRS"
            name = "GDI-BiProd02_disk2_0e9c12ec436e4198bd455a75396401fe"
            write_accelerator_enabled = false
        },
        {
            caching = "ReadOnly"
            create_option = "Attach"
            disk_size_gb = "1536"
            lun = "1"
            managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/GDI-RESGRPBI02/providers/Microsoft.Compute/disks/GDI-BiProd02_F1"
            managed_disk_type = "Premium_LRS"
            name = "GDI-BiProd02_F1"
            write_accelerator_enabled = false
        },
        {
            caching = "ReadOnly"
            create_option = "Attach"
            disk_size_gb = "1536"
            lun = "2"
            managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/GDI-RESGRPBI02/providers/Microsoft.Compute/disks/GDI-BiProd02_F2"
            managed_disk_type = "Premium_LRS"
            name = "GDI-BiProd02_F2"
            write_accelerator_enabled = false
        },
        {
            caching = "ReadOnly"
            create_option = "Attach"
            disk_size_gb = "1536"
            lun = "3"
            managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/GDI-RESGRPBI02/providers/Microsoft.Compute/disks/GDI-BiProd02_F3"
            managed_disk_type = "Premium_LRS"
            name = "GDI-BiProd02_F3"
            write_accelerator_enabled = false
        },
        {
            caching = "ReadOnly"
            create_option = "Attach"
            disk_size_gb = "1536"
            lun = "4"
            managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/GDI-RESGRPBI02/providers/Microsoft.Compute/disks/GDI-BiProd02_F4"
            managed_disk_type = "Premium_LRS"
            name = "GDI-BiProd02_F4"
            write_accelerator_enabled = false
        },
        {
            caching = "ReadOnly"
            create_option = "Attach"
            disk_size_gb = "1536"
            lun = "5"
            managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/GDI-RESGRPBI02/providers/Microsoft.Compute/disks/GDI-BiProd02_F5"
            managed_disk_type = "Premium_LRS"
            name = "GDI-BiProd02_F5"
            write_accelerator_enabled = false
        },
        {
            caching = "ReadOnly"
            create_option = "Attach"
            disk_size_gb = "1536"
            lun = "6"
            managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/GDI-RESGRPBI02/providers/Microsoft.Compute/disks/GDI-BiProd02_F6"
            managed_disk_type = "Premium_LRS"
            name = "GDI-BiProd02_F6"
            write_accelerator_enabled = false
        },
        {
            caching = "ReadOnly"
            create_option = "Attach"
            disk_size_gb = "1536"
            lun = "7"
            managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/GDI-RESGRPBI02/providers/Microsoft.Compute/disks/GDI-BiProd02_F7"
            managed_disk_type = "Premium_LRS"
            name = "GDI-BiProd02_F7"
            write_accelerator_enabled = false
        },
        {
            caching = "ReadOnly"
            create_option = "Attach"
            disk_size_gb = "1536"
            lun = "8"
            managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/GDI-RESGRPBI02/providers/Microsoft.Compute/disks/GDI-BiProd02_F8"
            managed_disk_type = "Premium_LRS"
            name = "GDI-BiProd02_F8"
            write_accelerator_enabled = false
        }
    ]
    storage_os_disk = {
        caching = "ReadWrite"
        create_option = "Attach"
        disk_size_gb = 127
        managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/Gdi-ResGrpBi02-VM-Backup/providers/Microsoft.Compute/disks/gdi_biprod02_OSDisk"
        managed_disk_type = "Premium_LRS"
        name = "gdi_biprod02_OSDisk"
        os_type = "Windows"
        write_accelerator_enabled = false
    }
    tags = {
        BI = "VM"
    }
    vm_size = "Standard_E32-16s_v3"
    delete_data_disks_on_termination = ""
    delete_os_disk_on_termination = ""
}

resource "azurerm_virtual_machine" "SIADCZ03" {
    boot_diagnostics = {
        enabled = true
        storage_uri = "https://gdiresgrpbi02diag741.blob.core.windows.net/"
    }
    location = "canadaeast"
    name = "SIADCZ03"
    network_interface_ids = [
        "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/Gdi-ResGrp01/providers/Microsoft.Network/networkInterfaces/siadcz03319"
    ]
    os_profile = {
        admin_username = "terminator"
        computer_name = "SIADCZ03"
    }
    os_profile_windows_config = {
        enable_automatic_upgrades = true
        provision_vm_agent = true
    }
    resource_group_name = "Gdi-ResGrp01"
    storage_image_reference = {
        offer = "WindowsServer"
        publisher = "MicrosoftWindowsServer"
        sku = "2008-R2-SP1"
        version = "latest"
    }
    storage_os_disk = {
        caching = "ReadWrite"
        create_option = "FromImage"
        disk_size_gb = 127
        managed_disk_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/GDI-RESGRP01/providers/Microsoft.Compute/disks/SIADCZ03_OsDisk_1_05100134ffaf4d04840914e22696dc3a"
        managed_disk_type = "Premium_LRS"
        name = "SIADCZ03_OsDisk_1_05100134ffaf4d04840914e22696dc3a"
        os_type = "Windows"
        write_accelerator_enabled = false
    }
    tags = {
        infra = "Network"
    }
    vm_size = "Standard_B2s"
    delete_data_disks_on_termination = ""
    delete_os_disk_on_termination = ""
}