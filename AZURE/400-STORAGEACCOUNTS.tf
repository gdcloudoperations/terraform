resource "azurerm_storage_account" "cs247bad6360982x4da7xb24" {
  account_encryption_source = "Microsoft.Storage"
  account_kind = "Storage"
  account_tier = "Standard"
  account_replication_type = "LRS"
  enable_blob_encryption = "true"
  enable_file_encryption = "true"
  enable_https_traffic_only = "false"
  location = "eastus"
  name = "cs247bad6360982x4da7xb24"
  resource_group_name = "cloud-shell-storage-eastus"
  tags = {
    ms-resource-usage = "azure-cloud-shell"
  }
}
resource "azurerm_storage_account" "biprod02databackup" {
  account_encryption_source = "Microsoft.Storage"
  account_kind = "Storage"
  account_tier = "Standard"
  account_replication_type = "LRS"
  enable_blob_encryption = "true"
  enable_file_encryption = "true"
  enable_https_traffic_only = "false"
  location = "canadaeast"
  name = "biprod02databackup"
  resource_group_name = "Gdi-ResGrpBi02-Backup"
  tags = {
    BI = "Storage"
  }
}
resource "azurerm_storage_account" "creative01" {
  account_encryption_source = "Microsoft.Storage"
  account_kind = "Storage"
  account_tier = "Standard"
  account_replication_type = "GRS"
  enable_blob_encryption = "true"
  enable_file_encryption = "true"
  enable_https_traffic_only = "false"
  location = "eastus"
  name = "creative01"
  resource_group_name = "FileServer"
  tags{
    FileServer = "GDI File Server"
  }
}
resource "azurerm_storage_account" "gdiresgrpbi02diag741" {
  account_encryption_source = "Microsoft.Storage"
  account_kind = "Storage"
  account_tier = "Standard"
  account_replication_type = "LRS"
  enable_blob_encryption = "true"
  enable_file_encryption = "true"
  enable_https_traffic_only = "false"
  location = "canadaeast"
  name = "gdiresgrpbi02diag741"
  resource_group_name = "Gdi-ResGrpBi02"
  tags{
    BI = "Storage"
  }
}
resource "azurerm_storage_account" "gdisimplstorfs" {
  account_encryption_source = "Microsoft.Storage"
  account_kind = "Storage"
  account_tier = "Standard"
  account_replication_type = "RAGRS"
  enable_blob_encryption = "true"
  enable_file_encryption = "true"
  enable_https_traffic_only = "true"
  location = "canadaeast"
  name = "gdisimplstorfs"
  resource_group_name = "NewResourceGroup"
  tags{
    FileServer = "GDI File Server"
  }
}
resource "azurerm_storage_account" "gdistorage01" {
  account_encryption_source = "Microsoft.Storage"
  account_kind = "Storage"
  account_tier = "Standard"
  account_replication_type = "LRS"
  enable_blob_encryption = "true"
  enable_file_encryption = "true"
  enable_https_traffic_only = "false"
  location = "canadaeast"
  name = "gdistorage01"
  network_rules{
    bypass = ["AzureServices"]
    ip_rules = ["216.113.37.178", "40.86.226.166"]
  }
  resource_group_name = "Gdi-ResGrp01"
  tags{
    BI = "Storage"
  }
}
resource "azurerm_storage_account" "gdistorage1" {
  account_encryption_source = "Microsoft.Storage"
  account_kind = "Storage"
  account_tier = "Standard"
  account_replication_type = "LRS"
  account_tier = "Premium"
  enable_blob_encryption = "true"
  enable_file_encryption = "true"
  enable_https_traffic_only = "true"
  location = "canadaeast"
  name = "gdistorage1"
  resource_group_name = "Gdi-ResGrp01"
  tags{
    BI = "Storage"
  }
}
resource "azurerm_storage_account" "gdistorageinfra" {
  account_encryption_source = "Microsoft.Storage"
  account_kind = "Storage"
  account_tier = "Standard"
  account_replication_type = "LRS"
  enable_blob_encryption = "true"
  enable_file_encryption = "true"
  enable_https_traffic_only = "false"
  location = "canadaeast"
  name = "gdistorageinfra"
  network_rules{
    bypass = ["AzureServices"]
    virtual_network_subnet_ids = [
      "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/Gdi-ResGrp01/providers/Microsoft.Network/virtualNetworks/GDI_VNET/subnets/SUB_SVR",
      "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/Gdi-ResGrp01/providers/Microsoft.Network/virtualNetworks/GDI_VNET/subnets/GatewaySubnet"
    ]
  }
  resource_group_name = "Gdi-Infra"
  tags{
    FileServer = "GDI File Server"
  }
}