resource "azurerm_virtual_network" "gdi_res_net-vnet" {
    address_space = ["10.0.0.0/24"]
    location = "eastus"
    name = "gdi.res.net-vnet"
    resource_group_name = "Gdi-ResGrp01"
    subnet = [
        {
            name = "default"
            address_prefix = "10.0.0.0/24"
        }
    ]
    tags{
        infra = "Network"
    }
}
resource "azurerm_virtual_network" "GDI_VNET" {
    address_space = ["172.26.0.0/16"]
    location = "canadaeast"
    name = "GDI_VNET"
    resource_group_name = "Gdi-ResGrp01"
    subnet = [
        {
            address_prefix = "172.26.0.0/27"
            name = "GatewaySubnet"
        },
        {
            address_prefix = "172.26.1.0/24"
            name = "SUB_SVR"
        }
    ]
    tags{
        infra = "Network"
    }
}
