resource "azurerm_virtual_network_gateway_connection" "GDI_VNETtoGDI540" {
    enable_bgp = "false"
    local_network_gateway_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/Gdi-ResGrp01/providers/Microsoft.Network/localNetworkGateways/GDI_5540"
    location = "canadaeast"
    name = "GDI_VNETtoGDI540"
    resource_group_name = "Gdi-ResGrp01"
    shared_key = "!Dyn@@zUr3.2018$$$$" #$$$$ is actually 2x$ and not 4x$
    tags{
        infra = "Network"
    }
    type = "IPsec"
    use_policy_based_traffic_selectors = "false"
    virtual_network_gateway_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/Gdi-ResGrp01/providers/Microsoft.Network/virtualNetworkGateways/VNet1GW"
}
resource "azurerm_virtual_network_gateway_connection" "VNet1GW-GCP_VPC_C0" {
    enable_bgp = "false"
    local_network_gateway_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/Gdi-ResGrp01/providers/Microsoft.Network/localNetworkGateways/GCP_VPC_C0"
    location = "canadaeast"
    name = "VNet1GW-GCP_VPC_C0"
    resource_group_name = "Gdi-ResGrp01"
    shared_key = "yFn0UsPuiaL1SyWV"
    type = "IPsec"
    use_policy_based_traffic_selectors = "false"
    virtual_network_gateway_id = "/subscriptions/47bad636-0982-4da7-b245-aba5478532cf/resourceGroups/Gdi-ResGrp01/providers/Microsoft.Network/virtualNetworkGateways/VNet1GW"
}