resource "azurerm_local_network_gateway" "GCP_VPC_C0" {
    address_space = ["172.20.254.0/24"]
    gateway_address = "35.196.59.202"
    location = "canadaeast"
    name = "GCP_VPC_C0"
    resource_group_name = "Gdi-ResGrp01"
}
resource "azurerm_local_network_gateway" "GDI_5540" {
    address_space = ["172.30.0.0/16", "172.31.0.0/16", "128.127.125.0/24", "10.50.10.0/24", "172.28.1.0/24"]
    gateway_address = "207.253.242.146"
    location = "canadaeast"
    name = "GDI_5540"
    resource_group_name = "Gdi-ResGrp01"
    tags = { 
        infra = "Network"
    }
}