resource "azurerm_resource_group" "AzureBackupRG_canadaeast_1" {
  name = "AzureBackupRG_canadaeast_1"
  location = "canadaeast"
}
resource "azurerm_resource_group" "cloud-shell-storage-eastus" {
  name = "cloud-shell-storage-eastus"
  location = "East US"
}
resource "azurerm_resource_group" "DataCatalogs-WestUs" {
  name = "DataCatalogs-WestUs"   
  location = "East US"
}
resource "azurerm_resource_group" "DefaultResourceGroup-EUS" {
  name = "DefaultResourceGroup-EUS"   
  location = "East US"
}
resource "azurerm_resource_group" "FileServer" {
  name = "FileServer"   
  location = "East US"
}
resource "azurerm_resource_group" "Gdi-Infra" {
  name = "Gdi-Infra"   
  location = "canadaeast"
}
resource "azurerm_resource_group" "Gdi-ResGrp01" {
  name = "Gdi-ResGrp01"   
  location = "canadaeast"
}
resource "azurerm_resource_group" "Gdi-ResGrpBi01" {
  name = "Gdi-ResGrpBi01"   
  location = "canadaeast"
}
resource "azurerm_resource_group" "Gdi-ResGrpBi02" {
  name = "Gdi-ResGrpBi02"   
  location = "canadaeast"
}
resource "azurerm_resource_group" "Gdi-ResGrpBi02-Backup" {
  name = "Gdi-ResGrpBi02-Backup"   
  location = "canadaeast"
}
resource "azurerm_resource_group" "Gdi-ResGrpBi02-VM-Backup" {
  name = "Gdi-ResGrpBi02-VM-Backup"   
  location = "canadaeast"
}
resource "azurerm_resource_group" "NetworkWatcherRG" {
  name = "NetworkWatcherRG"   
  location = "westus"
}
resource "azurerm_resource_group" "NewResourceGroup" {
  name = "NewResourceGroup"   
  location = "East US"
}
